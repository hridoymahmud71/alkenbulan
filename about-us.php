<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Denis Samardjiev" />
	<meta name="description" content="Raleway | Mega Bootstrap Template">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<title>Saving Partners</title>
	
	<!-- Royal Preloader CSS -->
	<link rel="stylesheet" type="text/css" href="css/royal_preloader.css">

	<!-- jQuery Files -->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

	<!-- Royal Preloader -->
	<script type="text/javascript" src="js/royal_preloader.min.js"></script>

	<!-- Revolution Slider CSS -->
	<link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
	<link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
		
	<!-- Revolution Slider Navigation CSS -->
	<link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">

	<!-- Stylesheets -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" title="main-css">
	<link href="css/bootstrap-social.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet" >
	<link href="css/jquery.snippet.css" rel="stylesheet">
	<link href="css/buttons.css" rel="stylesheet">

	<!-- Style Switcher / remove for production -->
	<link href="css/style-switcher.css" rel="stylesheet">
	
	<!-- Alternate Stylesheets / choose what color you want and include regularly AFTER style.css above -->
	<link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="blue">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/green.css" title="green">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/purple.css" title="purple">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/dark-blue.css" title="dark-blue">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/red.css" title="red">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/silver.css" title="silver">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/pinkish.css" title="pinkish">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/seagul.css" title="seagul">
	<link rel="alternate stylesheet" type="text/css" href="css/width-full.css" title="width-full">
	<link rel="alternate stylesheet" type="text/css" href="css/width-boxed.css" title="width-boxed">

	<!-- Icon Fonts -->
	<link href='css/ionicons.min.css' rel='stylesheet' type='text/css'>
	<link href='css/font-awesome.css' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Magnific Popup -->
	<link href='css/magnific-popup.css' rel='stylesheet' type='text/css'>
</head>
<body class="royal_preloader scrollreveal">

	<div id="royal_preloader"></div>
	
	

	<!-- Begin Boxed or Fullwidth Layout -->
	<div id="bg-boxed">
	    <div class="boxed">

			<!-- Begin Header -->
			<header>

				<!-- Begin Top Bar -->
				<div class="top-bar">
					<div class="container">
						<div class="row">
							<!-- Address and Phone -->
							<div class="col-sm-7 hidden-xs">
								<span class="ion-android-system-home home-icon" style="color: #00b7eb;"></span>dolor sit amet, conshxs, 63138 UAE<span class="ion-ios7-telephone phone-icon" style="color: #00b7eb;"></span>1754563921
							</div>
							<!-- Social Buttons -->
							<div class="col-sm-5 text-right">
				                <ul class="topbar-list list-inline">
					                <li>						
							            <a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-google-plus"></i>
										</a>
							            <a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-twitter"></i>
										</a>
										<a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-instagram"></i>
										</a>
										<a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
									<li><a href="login.html">Login</a></li>
									<li><a href="register.html">Register</a></li>
								</ul>
							</div>
						</div><!--/row --> 
					</div><!--/container header -->   
				</div><!--/top bar -->   
				<!-- End Top Bar -->

				<!-- Login -->
				<!-- <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
				    <div class="modal-dialog modal-sm">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				                <h4 class="modal-title" id="loginLabel">Login</h4>
				            </div>
				            <div class="modal-body">
					            <form role="form">
						            <div class="form-group">
						                <label for="exampleInputEmail1">Email address</label>
						                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						            </div>
						            <div class="form-group">
						                <label for="exampleInputPassword1">Password</label>
						                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
						            </div>
						            <div class="checkbox">
						                <label>
						                    <input type="checkbox"> Remember Me
						                </label>
						            </div>
						        </form>
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-rw btn-primary">Login</button>
				            </div>
				        </div>
				    </div>
				</div> -->
				<!-- End Login -->

		     	<!-- Begin Navigation -->
		     	<div class="navbar-wrapper">
					<div class="navbar navbar-main" id="fixed-navbar">
						<div class="container">
							<div class="row">
								<div class="col-sm-12 column-header">
									<div class="navbar-header">
										<!-- Brand -->
										<a href="index.html" class="navbar-brand">
											<h2 style=" color: #fff;">Saving Partners</h2> 
										</a>
										<!-- Mobile Navigation -->
										<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
								            <span class="sr-only">Toggle navigation</span>
								            <span class="icon-bar"></span>
								            <span class="icon-bar"></span>
								            <span class="icon-bar"></span>
										</button>
									</div><!-- /navbar header -->   

									<!-- Main Navigation - Explained in Documentation -->
									<nav class="navbar-collapse collapse navHeaderCollapse" role="navigation">
										<ul class="nav navbar-nav navbar-right">
											
									        <li >
									        	<a href="index.html" >Home</a>
									        	
									        </li>
									        <li >
									        	<a href="#" >About Us</a>
									        	
									        </li>
									        <li >
									        	<a href="#" >Services</a>
									        	
									        </li>
									        <li >
									        	<a href="membership.html" >Membership</a>
									        	
									        </li>
									        <li >
									        	<a href="#" >Joining Process</a>
									        	
									        </li>
									        <li >
									        	<a href="#" >Testimonial</a>
									        	
									        </li>
									         
									          <li >
									        	<a href="#" >Blog</a>
									        	
									        </li>
									         <li >
									        	<a href="#" >Contact</a>
									        	
									        </li>
									        
											<!--<li class="dropdown dropdown-main">
									        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog<span class="fa fa-angle-down dropdown-arrow"></span></a>
									        	<ul class="dropdown-menu dropdown-subhover dropdown-animation animated fadeIn">
									            	<li><a href="blog-posts-sidebar-right.html">Sidebar Right</a></li>
									            	<li><a href="blog-posts-sidebar-left.html">Sidebar Left</a></li>
									            	<li><a href="blog-posts-fullwidth.html">Full Width</a></li>
									            	<li><a href="blog-posts-grid.html">Grid</a></li>
									            	<li><a href="blog-single-post.html">Single Post</a></li>
									        	</ul>
									        </li>-->
											
										</ul><!-- /navbar right --> 
									</nav><!-- /nav -->
								</div>
							</div>
						</div><!-- /container header -->   
					</div><!-- /navbar -->
				</div><!-- /navbar wrapper -->
				<!-- End Navigation -->

			</header><!-- /header -->
			<!-- End Header -->

			<!-- Begin Content Section -->
			<section class="content-40mg">
				<div class="container">

					<!-- Begin Intro + Video -->
					<div class="row">
						<div class="col-sm-12">
							<p class="lead text-center flipInY-animated"><b><mark>About Us</mark></b></p>
							<hr style="width:600px">
						</div>
					</div>
					<div class="row">
						<!-- Content -->
						<div class="col-sm-6 fadeInLeft-animated">
							<div class="heading mb20"><h4><span class="ion-android-developer mr15 swing-animated"></span>We Are Saving Partners</h4></div>
							<p style="text-align:justify;">Portia Grant is the founder and managing director of Portia’s Partnership Savings Club, a UK-based first commercial Partner (Pardna) firm, dedicated to assisting low-income individuals and families in developing smart <b>pardoner savings</b> plans.</p>
							
							<p style="text-align:justify;">With a passion for assisting low income and struggling families, Portia, originally from Jamaica, has remained dedicated to serving those less fortunate for much of her life. In 2005, she started PPSP after meeting and bringing together five families in need of financial management advice. Since then, PPSP has grown to more than 500 members who are all experiencing first-hand the expertise and practical advice Portia has integrated into her saving plans or <b>monthly investment plans</b>, or what she has since dubbed the “Pardna Method.”</p>
							
							<p style="text-align:justify;">Portia’s aim is to provide a convenient and reliable Partner Scheme to individuals. Additionally, Portia believes that this system of saving money promotes responsibility as people learn to reserve a certain amount of their weekly/monthly income for savings.</p>
							 
						</div><!-- /column -->
						<!-- End Content -->

						<!-- Video -->
						<div class="col-sm-6 fadeInRight-animated">
							<img class="img-responsive" src="" />
						</div><!-- /video -->
					</div><!-- /row -->
					<!-- End Intro + Video -->

					<!-- Begin What We Do -->
					<div class="row mt20">
						<div class="col-sm-12">
							<div class="heading mb15"><h4>What Do We Do?</h4></div>
						</div>

						<!-- Content 1 -->
						<div class="col-sm-4">
						    <h4><span class="ion-beaker bordered-icon-sm mr15 bounce-animated"></span> We do parallax.</h4>
						    <p class="no-margin">Lorem ipsum dolor sit amet, consec tetur adipiscing elit. Integer a elit turpis. Phasellus non varius mi. Nam bibendum in mauris at sollicitudin lacinia.</p>
						</div>

						<!-- Content 2 -->
						<div class="col-sm-4 mt20-xs">
						    <h4><span class="ion-beer bordered-icon-sm mr15 bounce-animated"></span> We animate.</h4>
						    <p class="no-margin">Lorem ipsum dolor sit amet, consec tetur adipiscing elit. Integer a elit turpis. Phasellus non varius mi. Nam bibendum in mauris at sollicitudin lacinia.</p>
						</div>

						<!-- Content 3 -->
						<div class="col-sm-4 mt20-xs">
						    <h4><span class="ion-home bordered-icon-sm mr15 bounce-animated"></span> We like home.</h4>
						    <p class="no-margin">Lorem ipsum dolor sit amet, consec tetur adipiscing elit. Integer a elit turpis. Phasellus non varius mi. Nam bibendum in mauris at sollicitudin lacinia.</p>
						</div>
					</div>
					<!-- End What We Do -->

					<hr class="mt30 mb40">

					<!-- Begin We Are Dedicated -->
					<div class="row">
						<!-- Circle Thumbnail -->
						<div class="col-sm-2 col-xs-6 hidden-xs" data-sr="enter left over .5s">
							<a href="images/projects/web3.jpg" class="image-zoom-link thumbnail br-50">
							    <img class="img-circle img-responsive" src="images/projects/thumbs/web3.jpg" alt="Thumbnail">
							</a>
						</div>

						<!-- We Are Dedicated  -->
						<div class="col-sm-10 col-xs-12">
							<h4 data-sr="enter right over .5s">We Are Dedicated! <small class="hidden-xs">Not once have we had a lad quit.</small></h4>
							<p class="lead mb10" data-sr="enter right over .8s">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu risus libero. Donec et lorem lacinia, adipiscing nunc nec, hendrerit sapien. Sed in scelerisque tortor.</p>
							<p data-sr="enter right over 1s">Praesent porttitor odio non ullamcorper gravida. Donec bibendum risus risus, eu luctus mauris ullamcorper sagittis. Cras pretium pretium ligula, a adipiscing metus facilisis ac. Etiam ut orci a tortor tincidunt sollicitudin. Ut malesuada gravida enim in condimentum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sit amet vulputate arcu, eu pulvinar neque. Aliquam erat volutpat. Aenean euismod nisl sed justo pharetra, et pretium mauris porttitor. Mauris luctus justo eget tempus ornare. Pellentesque vitae sollicitudin ante.</p>
						</div>
					</div>
					<!-- End We Are Dedicated -->

				</div><!-- /container -->
			</section><!-- /section -->
			<!-- End Content Section -->

			<!-- End CTA -->
			<div class="cta2 arrow-up">
				<div class="container text-center">
					<h2>I am Raleway, a multi-purpose responsive template.</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc.</p>
				</div>
			</div><!-- /cta -->
			<!-- End CTA -->

			<!-- Begin Footer -->
			<footer class="footer">
			    <div class="container">
			        <div class="row">
			            <!-- About -->
			            <div class="col-sm-3">
			                <div class="heading-footer"><h4>About</h4></div>
			                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu risus libero. Donec et lorem lacinia, </p>
			                <!-- <a href="features.html" class="btn btn-primary btn-rw mt10">
			                    Buy now
			                </a> -->
			            </div>
			            
			            <!-- Social -->
			            <div class="col-sm-3 mg25-xs">
			                <div class="heading-footer"><h4>Social Networks</h4></div>
			                <a class="btn btn-social-icon btn-twitter btn-lg">
			                    <i class="fa fa-twitter"></i>
			                </a>
			                <a class="btn btn-social-icon btn-instagram btn-lg">
			                    <i class="fa fa-instagram"></i>
			                </a>
			                <a class="btn btn-social-icon btn-facebook btn-lg">
			                    <i class="fa fa-facebook"></i>
			                </a>
			                <a class="btn btn-social-icon btn-google-plus btn-lg">
			                    <i class="fa fa-google-plus"></i>
			                </a>
			                
			                <a class="btn btn-social-icon btn-linkedin btn-lg">
			                    <i class="fa fa-linkedin"></i>
			                </a>

			                <!-- <a class="btn btn-social-icon btn-vimeo btn-lg">
			                    <i class="fa fa-vimeo-square"></i>
			                </a>
			                <a class="btn btn-social-icon btn-adn btn-lg">
			                    <i class="fa fa-adn"></i>
			                </a>
			                <a class="btn btn-social-icon btn-bitbucket btn-lg">
			                    <i class="fa fa-bitbucket"></i>
			                </a>
			                <a class="btn btn-social-icon btn-tumblr btn-lg">
			                    <i class="fa fa-tumblr"></i>
			                </a>
			                <a class="btn btn-social-icon btn-flickr btn-lg">
			                    <i class="fa fa-flickr"></i>
			                </a> -->
			            </div>
			            <!-- Contact -->
			            <div class="col-sm-3 mg25-xs">
			                <div class="heading-footer"><h4>Contact us</h4></div>
			                <p><span class="ion-home footer-info-icons"></span><small class="address">dolor sit amet, conshxs, 63138 UAE</small></p>
			                <p><span class="ion-email footer-info-icons"></span><small class="address"><a href="mailto:info@savingpartners.com">info@savingpartners.com</a></small></p>
			                <p><span class="ion-ios7-telephone footer-info-icons"></span><small class="address">+1754563921</small></p>
			            </div>
			            <!-- Recent Work -->
			            <div class="col-sm-3 hidden-xs">
			                <div class="heading-footer"><h4>Recent Work</h4></div>
			                <div class="col-xs-4 recent-work-padding">
			                    <a href="images/projects/web2.jpg" class="thumbnail image-zoom-link">
			                      <img src="images/projects/thumbs/web2.jpg" alt="..." class="thumbnail img-responsive">
			                    </a>
			                </div>
			                <div class="col-xs-4 recent-work-padding">
			                    <a href="images/projects/web3.jpg" class="thumbnail image-zoom-link">
			                      <img src="images/projects/thumbs/web3.jpg" alt="..." class="thumbnail img-responsive">
			                    </a>
			                </div>
			                <div class="col-xs-4 recent-work-padding">
			                    <a href="images/projects/illustration1.jpg" class="thumbnail image-zoom-link">
			                      <img src="images/projects/thumbs/illustration1.jpg" alt="..." class="thumbnail img-responsive">
			                    </a>
			                </div>
			            </div>
			        </div><!-- /row -->
			        <!-- Copyright -->
			        <div class="row">
			            <hr class="dark-hr">
			            <div class="col-sm-11 col-xs-10">
			                <p class="copyright">© 2018 Saving Partners. All rights reserved. Designed by <b>Shrinkcom Softwares</b>.</p>
			            </div>
			            <div class="col-sm-1 col-xs-2 text-right">
			                <a href="#" class="scroll-top"><div class="footer-scrolltop-holder"><span class="ion-ios7-arrow-up footer-scrolltop"></span></div></a>
			            </div>
			        </div><!-- /row -->
			    </div><!-- /container -->
			</footer><!-- /footer -->
			<!-- End Footer -->

		</div><!-- /boxed -->
	</div><!-- /bg boxed-->
	<!-- End Boxed or Fullwidth Layout -->

	<!-- Javascript Files -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="js/scrollReveal.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>
	<script type="text/javascript" src="js/style-switcher.js"></script><!-- Remove for production -->
	<script type="text/javascript" src="js/activate-snippet.js"></script>
	<script type="text/javascript" src="js/skrollr.min.js"></script>

	<!-- On Scroll Animations - scrollReveal.js -->
    <script>
		var config = {
		easing: 'hustle',
		mobile:  true,
		delay:  'onload'
		}
		window.sr = new scrollReveal( config );
    </script>

    <!-- Main Javascript -->
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>