<?php
  include('header.php');
?>
        
      
   
</div>
<section style="background-image:url(./images/service-banner.jpg);">
	<h1 style="text-align:center; padding: 100px; color:#fff;">Services</h1>
</section>	


<section class="section-light" style="padding:50px;">
    <div class="divider-line solid light"></div>
    <div class="container-fluid">
      <div class="row sec-padding">
        
        <div class="col-md-11 col-centered">
		<div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 margin-bottom"> <img src="images/91.jpg" alt="" class="img-responsive"> </div>
       
        
        <div class="col-md-8 col-sm-6 col-xs-12">
          <div class="text-box white padding-4 margin-bottom">
            <h3 class="uppercase font-weight-b">OUR SERVICES</h3>
			<p>WE PROVIDE TWO DISTINCT SERVICES FOR YOU:</p>
			<p>Alkebulan Savings Club (ASC) provides its members a very easy and stress free way to save towards a ‘target’. This is an online service that aims to offer an alternative saving approach. At the moment members can choose any of our two distinct and unique saving plans-</p>
			<p><i class="fa fa-check"></i>The ‘Alkebulan Plan’<a href="https://alkebulan.london/choose_plan.php">(A-Plan)</a></p>
<p><i class="fa fa-check"></i>The Rotary (or Susu) Plan<a href="https://alkebulan.london/select_circle_group.php">(S-Plan) </a></p>
<p>
Please join the many registering to save with ASC. Our saving schemes affords our members the opportunity to raise finance for personal use or for investment.</p>
<p>You can register as an individual with the A-plan and save to a target or register with a group to join our S-plan
</p>
<h4 class="">THE A-PLAN</h4>
<p>The Alkebulan Plan is a flexible saving scheme where the member chooses a target and consistently saves towards that target. Your savings is available to you once that target is achieved.  That target is for you to fix. It can be a target of time – that is save consistently for a certain number of months or years, or a target in terms of an amount of money the customer hope to save towards.
ASC offers you the platform to put money away for a certain number of times to achieve a pre-determined target. This is our “Alkebulan Plan or A-Plan.”</p>
<div class="clearfix">&nbsp;</div>
<h4 class=" raleway font-weight-b less-mar-1 uppercase">This is how it works:</h4>


<p><i class="fa fa-check"></i> Open an ASC account in minutes.</p>
<p><i class="fa fa-check"></i> Choose your regular savings amount.</p>
<p><i class="fa fa-check"></i>Choose how often you will be depositing (eg weekly, monthly)</p>
<p><i class="fa fa-check"></i>Choose your intended target amount (ie your total savings).</p>
<p><i class="fa fa-check"></i> Choose your method of paid back once target is achieved.</p>
<p><i class="fa fa-check"></i> It’s that simple 
To start saving please <a href="https://alkebulan.london/register.php"> click here to register</a></p>


    </div>
       
        </div>
	  </div>
       
      </div>
    </div>
  </div></section>
  <div class="parallax vertical-align" style="padding: 70px; background-color: #000;">
   <div class="parallax-overlay bg-opacity-7">
          <div class="container sec-padding">
          <div class="row">
          
         <div class="col-md-6" style="border: 5px solid #fff; padding: 18px; color: #fff;
    background-color: #00b7eb;">
              <div class="ce-feature-box-24">
                <div class="text-box">
                  <div class="inner-box text-left">
                    <h2 style="
    color: #fff;
">WE OFFER THE FOLLOWING SERVICES:</h2>
<ul class="list-inline">
<br/>
<li><i class="fa fa-check"></i> THE A-PLAN (A-PLAN), This is a flexible saving scheme where the member chooses a target and regularly saves.
</li>
<li><br/> <i class="fa fa-check"></i> THE S-PLAN (S-PLAN), This creates a group savings and members collect their due savings at end of each cycle.
</li>
<li><br/> <i class="fa fa-check"></i> SHORT TERM LOANS ACS offers short term loans to our regular and trustworthy customers to help in times of need.
 <br/> <br/>  </li> 
           
			</div>
                 
                </div>
                
                
              </div>
            </div>
          
            
            
            <div class="col-md-6">
              <div class="ce-feature-box-24">
                <div class="text-box">
			<img src="trust.jpg" alt="" title="" class="img-responsive" style="
    height: 337px;
">
                 
                </div>
              </div>
            </div>
           
          
            
          </div>
      </div>
        </div>
    
</div>
  <section class="section-side-image section-light clearfix">
      <div class="img-holder col-md-6 col-sm-3 pull-left">
        <div class="background-imgholder" style="images/service-big.jpg);"><img class="nodisplay-image" src="images/service-big.jpg" alt="" style="width:100%;"> </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 text-inner-2 pull-right clearfix align-left">
            <div class="text-box white padding-3">
              <div class="ce3-feature-box-7">
               
                
               


<div class="clearfix">&nbsp;</div>
<p>&nbsp;</p>
<h4 class="">THE S-PLAN</h4>

<p>The Rotating Savings or SUSU scheme is the type of savings where two or more friends/family members or colleagues from work will come together and agree to save a set amount collectively and consistently. At an agree period one of the members collect the total saving and the cycle starts again till all members within the groups get their due total savings.
This traditional way of saving is what the ASC has modernised to allow people and friends across the globe, through online banking, to come together while saving towards their intended targets. This is our “Susu Plan or S-Plan.”</p>
<div class="clearfix">&nbsp;</div>
<p>&nbsp;</p>

 <h4 class=" raleway font-weight-b less-mar-1 uppercase">This is how it works:</h4>
.
<p><i class="fa fa-check"></i>Group members contribute a pre-agreed regular payment.</p>
<p><i class="fa fa-check"></i>The banker consolidates all payments for that week or month into a draw.</p>
<p><i class="fa fa-check"></i>After a round of contribution, one member of the group receives the total amount contributed. </p> 
<p><i class="fa fa-check"></i>The exact amount received will be minus one procent of contribution, which is the commission of ASC.</p>
<p><i class="fa fa-check"></i>The group cannot add or take away a member once contributions start</p><p><i class="fa fa-check"></i>A Member may choose to be in several groups or make more than one contribution within same group.</p>
<p><i class="fa fa-check"></i>It's that simple. Get your friends, family members or colleagues to start our S-plan now.</p>
 <div class="clearfix"></div><br>
                <h4 class=" raleway font-weight-b less-mar-1 uppercase">HOW OUR SERVICES COULD BENEFT YOU:</h4>
Alkebulan Savings Club (ASC) provides its members a very easy and stress fun way to start savings online. The main aim is to offer a platform for its members to save consistently towards any target they set. The Alkebulan Plan helps you achieve a large savings goal.This is an online service that aims to offer an alternative saving approach. At the moment members can choose between one of our two distinct and unique saving plans-</p>
 <div class="clearfix">&nbsp;</div>
<p>&nbsp;</p>





				
               
				</div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php
  include('footer.php');
?>