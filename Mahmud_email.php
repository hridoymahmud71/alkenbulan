<?php
/**
 * Created by PhpStorm.
 * User: Mahmud
 * Date: 2019-02-13
 * Time: 6:40 PM
 */

class Mahmud_email
{

    public function __construct()
    {

    }

    public function default_sender()
    {
        $default_sender = "alkebulan-noreply@wg.rssoft.win"; //maybe get from some config file

        return $default_sender;
    }

    public function send($mail_data)
    {
        $default_sender = $this->default_sender();

        $headers = "";
        $headers .= isset($mail_data['from']) ? "From: {$mail_data['from']}\r\n" : "From: {$default_sender}\r\n";

        $to = isset($mail_data['to']) ? (is_array($mail_data['to']) ? implode(', ', $mail_data['to']) : $mail_data['to']) : "";
        $subject = isset($mail_data['subject']) ? $mail_data['subject'] : "";
        $message = isset($mail_data['message']) ? $mail_data['message'] : "";

        $mail_type = isset($mail_data['mail_type']) ? $mail_data['mail_type'] : "html";
        $headers .= $mail_type == "html" ? $this->headers_for_html() : "";
        $headers .= "X-Priority: 1 (Highest)\n";

        $mail_sent = array();
        $mail_sent['sent'] = false;
        $mail_sent['message'] = "";
        // send email

        try {
            mail($to, $subject, $message, $headers);
            $mail_sent['sent'] = true;
            $mail_sent['message'] = "Mail Sent!";
        } catch (Exception $e) {
            $mail_sent['sent'] = false;
            $mail_sent['message'] = "Mail Send Failed! (".$e->getMessage().")";
        }

        return $mail_sent;

    }

    public function headers_for_html()
    {
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        return $headers;
    }
}