			<!-- Begin Footer -->
			<footer class="footer">
			    <div class="container">
			        <div class="row">
			            <!-- About -->
			            <div class="col-sm-3">
			                <div class="heading-footer"><h4>About</h4></div>
			                <p>ASC presents an alternative saving scheme to that provided by the high street banks.</p>
			                <!-- <a href="features.php" class="btn btn-primary btn-rw mt10">
			                    Buy now
			                </a> -->
			            </div>
			            
			            <!-- Social -->
			            <div class="col-sm-3 mg25-xs">
			                <div class="heading-footer"><h4>Social Networks</h4></div>
			                <a class="btn btn-social-icon btn-twitter btn-lg">
			                    <i class="fa fa-twitter"></i>
			                </a>
			                <a class="btn btn-social-icon btn-instagram btn-lg">
			                    <i class="fa fa-instagram"></i>
			                </a>
			                <a class="btn btn-social-icon btn-facebook btn-lg">
			                    <i class="fa fa-facebook"></i>
			                </a>
			                <a class="btn btn-social-icon btn-google-plus btn-lg">
			                    <i class="fa fa-google-plus"></i>
			                </a>
			                
			                <a class="btn btn-social-icon btn-linkedin btn-lg">
			                    <i class="fa fa-linkedin"></i>
			                </a>

			                <!-- <a class="btn btn-social-icon btn-vimeo btn-lg">
			                    <i class="fa fa-vimeo-square"></i>
			                </a>
			                <a class="btn btn-social-icon btn-adn btn-lg">
			                    <i class="fa fa-adn"></i>
			                </a>
			                <a class="btn btn-social-icon btn-bitbucket btn-lg">
			                    <i class="fa fa-bitbucket"></i>
			                </a>
			                <a class="btn btn-social-icon btn-tumblr btn-lg">
			                    <i class="fa fa-tumblr"></i>
			                </a>
			                <a class="btn btn-social-icon btn-flickr btn-lg">
			                    <i class="fa fa-flickr"></i>
			                </a> -->
			            </div>
			            <!-- Contact -->
			            <div class="col-sm-3 mg25-xs">
			                <div class="heading-footer"><h4>Contact us</h4></div>
			                <p><span class="ion-home footer-info-icons"></span><small class="address">24 Page Avenue
Wembley, Middlesex
HA9 9FP UK</small></p>
			                <p><span class="ion-email footer-info-icons"></span><small class="address"><a href="mailto:notfol@hotmail.com">admin@alkebulan.london;notfol@hotmail.com</a></small></p>
			                <p><span class="ion-ios7-telephone footer-info-icons"></span><small class="address">(+44) 7533695264</small></p>
			            </div>
			            <!-- Recent Work -->
			            <div class="col-sm-3 hidden-xs">
			                <div class="heading-footer"><h4>Recent Work</h4></div>
			                <div class="col-xs-4 recent-work-padding">
			                    <a href="images/projects/web2.jpg" class="thumbnail image-zoom-link">
			                      <img src="images/projects/thumbs/web2.jpg" alt="..." class="thumbnail img-responsive">
			                    </a>
			                </div>
			                <div class="col-xs-4 recent-work-padding">
			                    <a href="images/projects/web3.jpg" class="thumbnail image-zoom-link">
			                      <img src="images/projects/thumbs/web3.jpg" alt="..." class="thumbnail img-responsive">
			                    </a>
			                </div>
			                <div class="col-xs-4 recent-work-padding">
			                    <a href="images/projects/illustration1.jpg" class="thumbnail image-zoom-link">
			                      <img src="images/projects/thumbs/illustration1.jpg" alt="..." class="thumbnail img-responsive">
			                    </a>
			                </div>
			            </div>
			        </div><!-- /row -->
			        <!-- Copyright -->
			        <div class="row">
			            <hr class="dark-hr">
			            <div class="col-sm-11 col-xs-10">
			                <p class="copyright">© 2018 Alkebulan Savings Club & Partners. All rights reserved. Designed by <b>Shrinkcom Softwares</b>.</p>
			            </div>
			            <div class="col-sm-1 col-xs-2 text-right">
			                <a href="#" class="scroll-top"><div class="footer-scrolltop-holder"><span class="ion-ios7-arrow-up footer-scrolltop"></span></div></a>
			            </div>
			        </div><!-- /row -->
			    </div><!-- /container -->
			</footer><!-- /footer -->
			<!-- End Footer --> 

		</div><!-- /boxed -->
	</div><!-- /bg boxed-->
	<!-- End Boxed or Fullwidth Layout -->
	
	<!-- Javascript Files -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="js/scrollReveal.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>
	<script type="text/javascript" src="js/style-switcher.js"></script><!-- Remove for production -->
	<script type="text/javascript" src="js/activate-snippet.js"></script>
	<script type="text/javascript" src="js/skrollr.min.js"></script>

	<!-- On Scroll Animations - scrollReveal.js -->
    <script>
		var config = {
		easing: 'hustle',
		mobile:  true,
		delay:  'onload'
		}
		window.sr = new scrollReveal( config );
    </script>

	<!-- Slider Revolution JS -->
	<script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>

	<!-- Slider Revolution Extensions (Load Extensions only on Local File Systems The following part can be removed on Server for On Demand Loading) -->	
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<!-- Slider Revolution Main -->
	<!-- <script type="text/javascript">
	jQuery(document).ready(function() { 
	   jQuery("#slider1").revolution({
	        sliderType:"standard",
	        startDelay:2500,
	        spinner:"spinner2",
	        sliderLayout:"auto",
	        viewPort:{
	           enable:false,
	           outof:'wait',
	           visible_area:'100%'
	        }
	        ,
	        delay:9000,
			navigation: {
				keyboardNavigation:"off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation:"off",
				onHoverStop:"off",
				arrows: {
					style:"erinyen",
					enable:true,
					hide_onmobile:true,
					hide_under:600,
					hide_onleave:true,
					hide_delay:200,
					hide_delay_mobile:1200,
					tmp:'<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div>    <div class="tp-arr-img-over"></div>	<span class="tp-arr-titleholder">{{title}}</span> </div>',
					left: {
						h_align:"left",
						v_align:"center",
						h_offset:30,
						v_offset:0
					},
					right: {
						h_align:"right",
						v_align:"center",
						h_offset:30,
						v_offset:0
					}
				}
				,
				touch:{
					touchenabled:"on",
					swipe_treshold : 75,
					swipe_min_touches : 1,
					drag_block_vertical:false,
					swipe_direction:"horizontal"
				}
				,
				bullets: {
	                enable:true,
	                hide_onmobile:true,
	                hide_under:600,
	                style:"hermes",
	                hide_onleave:true,
	                hide_delay:200,
	                hide_delay_mobile:1200,
	                direction:"horizontal",
	                h_align:"center",
	                v_align:"bottom",
	                h_offset:0,
	                v_offset:30,
	                space:5
				}
			},
			gridwidth:1240,
			gridheight:497 
	    }); 
	}); 
	</script> -->
	
	   <script type="text/javascript" src="js/main.js"></script>
    
    <script>
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 8000);

</script>  


   <script>
   
   $( document ).ready(function() {

$('img').bind('contextmenu', function(e) {
    return false;
});
       
   });


</script>  
</body>
</html>