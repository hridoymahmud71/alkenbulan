<?php

include_once('Mahmud_query.php');
include_once('Settings.php');

Class Mahmud_utility
{
    public $mq;
    public $settings;

    public function __construct()
    {
        $this->mq = new Mahmud_query();
        $this->settings = new Settings();

    }


    public function current_link($without_param = false)
    {
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        return $without_param ? $this->remove_params_from_url($actual_link) : $actual_link;
    }

    public function remove_params_from_url($url)
    {
        $parts = explode('?', $url);

        return isset($parts[0]) ? $parts[0] : $url;
    }

    // not working properly
    public function redirect($url)
    {
        ob_end_clean();
        header("Location: {$url}");
        die();
    }

    public function js_redirect($url)
    {
        echo "<script>window.location ='{$url}'</script>";
        exit;
    }


    public function getUser($user_id = false)
    {
        $sql = "SELECT * FROM `register_user` WHERE `id`='{$user_id}' ";

        $user = $this->mq->row($sql);

        return $user;
    }

    public function stripe_customer_exists($user_id)
    {
        $exists = false;

        $user_stripe_customer = $this->user_stripe_customer($user_id);
        if (!empty($user_stripe_customer)) {
            if (!empty($user_stripe_customer['stripe_customer_id'])) {
                $exists = true;
            }
        }
        return $exists;
    }

    public function user_stripe_customer($user_id)
    {
        $sql = "SELECT * FROM `stripe_customer` WHERE `user_id`='{$user_id}' ";

        $row = $this->mq->row($sql);

        return $row;
    }


}