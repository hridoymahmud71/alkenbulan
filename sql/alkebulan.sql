-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2019 at 03:35 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alkebulan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(55) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` int(55) NOT NULL DEFAULT '0',
  `status` int(55) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `password`, `type`, `status`) VALUES
(1, 'admin', 'password', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(255) NOT NULL,
  `create_userid` varchar(255) NOT NULL,
  `group_memberid` varchar(255) NOT NULL,
  `group_amount` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message_user`
--

CREATE TABLE `message_user` (
  `id` int(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `period`
--

CREATE TABLE `period` (
  `id` int(255) NOT NULL,
  `period` varchar(255) DEFAULT NULL,
  `type_id` varchar(255) DEFAULT NULL,
  `id_saving_plan` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `period`
--

INSERT INTO `period` (`id`, `period`, `type_id`, `id_saving_plan`, `status`) VALUES
(46, '4 month', '10', 2, 0),
(45, '5 month', '10', 2, 0),
(7, '3 month', '3', 1, 0),
(8, '4 month', '3', 1, 0),
(47, '6 month', '10', 2, 0),
(48, '5 month', '3', 1, 0),
(14, '12 week', '6', 1, 0),
(49, '6 month', '3', 1, 0),
(50, '3 month', '10', 2, 0),
(52, '7 month', '10', 2, 0),
(53, '8 month', '10', 2, 0),
(54, '9 month', '10', 2, 0),
(55, '10 month', '10', 2, 0),
(56, '7 months', '3', 1, 0),
(57, '8 months', '3', 1, 0),
(58, '9 months', '3', 1, 0),
(59, '10 months', '3', 1, 0),
(60, '11 months', '3', 1, 0),
(61, '12 months', '3', 1, 0),
(62, '24 months', '3', 1, 0),
(63, '36 months', '3', 1, 0),
(64, '18 months', '3', 1, 0),
(65, '11 month', '10', 2, 0),
(66, '12 month', '10', 2, 0),
(67, '3 month', '10', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id` int(255) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `type_id` varchar(255) DEFAULT NULL,
  `period_id` varchar(255) DEFAULT NULL,
  `id_saving_plan` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `price`, `type_id`, `period_id`, `id_saving_plan`, `status`) VALUES
(31, 200, '10', '25', 2, 0),
(32, 300, '10', '25', 2, 0),
(33, 400, '10', '25', 2, 0),
(43, 200, '10', '', 2, 0),
(44, 200, '10', '24', 2, 0),
(65, 150, '3', '8', 1, 0),
(66, 250, '3', '8', 1, 0),
(68, 350, '3', '8', 1, 0),
(69, 400, '3', '8', 1, 0),
(72, 550, '3', '8', 1, 0),
(234, 100, '3', '7', 1, 0),
(233, 50, '3', '7', 1, 0),
(232, 100, '3', '49', 1, 0),
(231, 150, '10', '45', 2, 0),
(230, 150, '10', '46', 2, 0),
(228, 150, '3', '48', 1, 0),
(89, 500, '3', '35', 1, 0),
(90, 100, '3', '36', 1, 0),
(91, 150, '3', '36', 1, 0),
(92, 200, '3', '36', 1, 0),
(93, 200, '3', '36', 1, 0),
(94, 250, '3', '36', 1, 0),
(95, 300, '3', '36', 1, 0),
(96, 350, '3', '36', 1, 0),
(97, 400, '3', '36', 1, 0),
(98, 450, '3', '36', 1, 0),
(99, 500, '3', '36', 1, 0),
(100, 100, '3', '37', 1, 0),
(101, 100, '3', '37', 1, 0),
(102, 150, '3', '37', 1, 0),
(103, 200, '3', '37', 1, 0),
(104, 250, '3', '37', 1, 0),
(105, 300, '3', '37', 1, 0),
(106, 350, '3', '37', 1, 0),
(107, 400, '3', '37', 1, 0),
(108, 450, '3', '37', 1, 0),
(109, 500, '3', '37', 1, 0),
(110, 100, '3', '38', 1, 0),
(111, 150, '3', '38', 1, 0),
(112, 200, '3', '38', 1, 0),
(113, 250, '3', '38', 1, 0),
(114, 300, '3', '38', 1, 0),
(115, 350, '3', '38', 1, 0),
(116, 400, '3', '38', 1, 0),
(117, 450, '3', '38', 1, 0),
(118, 500, '3', '38', 1, 0),
(119, 100, '3', '11', 1, 0),
(120, 150, '3', '11', 1, 0),
(121, 250, '3', '11', 1, 0),
(122, 300, '3', '11', 1, 0),
(123, 350, '3', '11', 1, 0),
(124, 400, '3', '11', 1, 0),
(125, 450, '3', '11', 1, 0),
(126, 500, '3', '11', 1, 0),
(127, 100, '3', '39', 1, 0),
(128, 150, '3', '39', 1, 0),
(129, 150, '3', '39', 1, 0),
(130, 200, '3', '39', 1, 0),
(131, 250, '3', '39', 1, 0),
(132, 300, '3', '39', 1, 0),
(133, 350, '3', '39', 1, 0),
(134, 400, '3', '39', 1, 0),
(135, 450, '3', '39', 1, 0),
(136, 500, '3', '39', 1, 0),
(137, 100, '3', '40', 1, 0),
(138, 150, '3', '40', 1, 0),
(139, 200, '3', '40', 1, 0),
(140, 250, '3', '40', 1, 0),
(141, 300, '3', '40', 1, 0),
(142, 350, '3', '40', 1, 0),
(143, 400, '3', '40', 1, 0),
(144, 450, '3', '40', 1, 0),
(145, 500, '3', '40', 1, 0),
(146, 150, '10', '21', 2, 0),
(147, 250, '10', '21', 2, 0),
(148, 350, '10', '21', 2, 0),
(149, 100, '10', '21', 2, 0),
(150, 400, '10', '21', 2, 0),
(151, 450, '10', '21', 2, 0),
(152, 150, '10', '23', 2, 0),
(153, 100, '10', '23', 2, 0),
(154, 250, '10', '23', 2, 0),
(155, 300, '10', '23', 2, 0),
(156, 350, '10', '23', 2, 0),
(157, 400, '10', '23', 2, 0),
(158, 500, '10', '23', 2, 0),
(159, 450, '10', '23', 2, 0),
(160, 100, '10', '24', 2, 0),
(161, 150, '10', '24', 2, 0),
(162, 250, '10', '24', 2, 0),
(163, 300, '10', '24', 2, 0),
(164, 350, '10', '24', 2, 0),
(165, 400, '10', '24', 2, 0),
(166, 500, '10', '24', 2, 0),
(167, 450, '10', '24', 2, 0),
(168, 100, '10', '25', 2, 0),
(169, 150, '10', '25', 2, 0),
(170, 250, '10', '25', 2, 0),
(171, 450, '10', '25', 2, 0),
(172, 500, '10', '25', 2, 0),
(173, 350, '10', '25', 2, 0),
(174, 100, '10', '26', 2, 0),
(175, 150, '10', '26', 2, 0),
(176, 200, '10', '26', 2, 0),
(177, 250, '10', '26', 2, 0),
(229, 150, '10', '47', 2, 0),
(179, 400, '10', '26', 2, 0),
(180, 450, '10', '26', 2, 0),
(181, 500, '10', '26', 2, 0),
(182, 450, '10', '27', 2, 0),
(183, 100, '10', '27', 2, 0),
(184, 150, '10', '27', 2, 0),
(185, 250, '10', '27', 2, 0),
(186, 350, '10', '27', 2, 0),
(187, 400, '10', '27', 2, 0),
(188, 500, '10', '27', 2, 0),
(189, 100, '10', '28', 2, 0),
(190, 150, '10', '28', 2, 0),
(191, 200, '10', '28', 2, 0),
(192, 350, '10', '28', 2, 0),
(193, 300, '10', '28', 2, 0),
(194, 250, '10', '28', 2, 0),
(195, 400, '10', '28', 2, 0),
(196, 450, '10', '28', 2, 0),
(197, 500, '10', '28', 2, 0),
(198, 100, '10', '20', 2, 0),
(199, 150, '10', '20', 2, 0),
(200, 250, '10', '20', 2, 0),
(201, 350, '10', '20', 2, 0),
(202, 450, '10', '20', 2, 0),
(203, 500, '10', '20', 2, 0),
(204, 300, '10', '20', 2, 0),
(205, 400, '10', '20', 2, 0),
(206, 100, '10', '29', 2, 0),
(207, 150, '10', '29', 2, 0),
(208, 200, '10', '29', 2, 0),
(209, 250, '10', '29', 2, 0),
(210, 350, '10', '29', 2, 0),
(211, 450, '10', '29', 2, 0),
(212, 500, '10', '29', 2, 0),
(213, 400, '10', '29', 2, 0),
(214, 100, '10', '41', 2, 0),
(215, 150, '10', '42', 2, 0),
(216, 200, '10', '41', 2, 0),
(217, 250, '10', '41', 2, 0),
(218, 300, '10', '41', 2, 0),
(219, 150, '10', '41', 2, 0),
(220, 350, '10', '41', 2, 0),
(221, 400, '10', '41', 2, 0),
(222, 500, '10', '41', 2, 0),
(223, 450, '10', '29', 2, 0),
(224, 250, '10', '41', 2, 0),
(225, 10, '2', '43', 1, 0),
(226, 10, '2', '43', 1, 0),
(227, 250, '3', '35', 1, 0),
(350, 100, '10', '52', 2, 0),
(236, 300, '10', '52', 2, 0),
(237, 250, '10', '53', 2, 0),
(238, 250, '10', '54', 2, 0),
(239, 250, '10', '55', 2, 0),
(240, 250, '10', '50', 2, 0),
(241, 100, '3', '56', 1, 0),
(242, 250, '3', '8', 1, 0),
(243, 300, '3', '49', 1, 0),
(244, 500, '3', '49', 1, 0),
(245, 500, '10', '50', 2, 0),
(246, 250, '10', '45', 2, 0),
(247, 300, '10', '45', 2, 0),
(248, 200, '3', '7', 1, 0),
(249, 300, '3', '7', 1, 0),
(250, 100, '3', '62', 1, 0),
(251, 50, '3', '62', 1, 0),
(252, 150, '3', '62', 1, 0),
(253, 200, '3', '62', 1, 0),
(254, 50, '3', '64', 1, 0),
(255, 100, '3', '63', 1, 0),
(256, 200, '3', '64', 1, 0),
(257, 50, '3', '63', 1, 0),
(258, 150, '3', '63', 1, 0),
(259, 10, '3', '7', 1, 0),
(260, 200, '3', '48', 1, 0),
(261, 100, '3', '61', 1, 0),
(263, 150, '3', '61', 1, 0),
(264, 250, '3', '61', 1, 0),
(265, 50, '3', '61', 1, 0),
(266, 300, '3', '61', 1, 0),
(267, 50, '3', '60', 1, 0),
(268, 100, '3', '60', 1, 0),
(269, 150, '3', '60', 1, 0),
(270, 200, '3', '60', 1, 0),
(271, 250, '3', '60', 1, 0),
(272, 300, '3', '60', 1, 0),
(273, 50, '3', '59', 1, 0),
(274, 150, '3', '59', 1, 0),
(275, 200, '3', '59', 1, 0),
(276, 250, '3', '59', 1, 0),
(277, 300, '3', '59', 1, 0),
(278, 50, '3', '58', 1, 0),
(279, 100, '3', '58', 1, 0),
(280, 150, '3', '58', 1, 0),
(281, 200, '3', '58', 1, 0),
(282, 250, '3', '58', 1, 0),
(283, 300, '3', '58', 1, 0),
(284, 50, '3', '57', 1, 0),
(285, 100, '3', '57', 1, 0),
(286, 150, '3', '57', 1, 0),
(287, 200, '3', '57', 1, 0),
(288, 250, '3', '57', 1, 0),
(289, 300, '3', '57', 1, 0),
(290, 50, '3', '56', 1, 0),
(291, 150, '3', '56', 1, 0),
(292, 100, '3', '59', 1, 0),
(293, 200, '3', '56', 1, 0),
(294, 250, '3', '56', 1, 0),
(295, 300, '3', '56', 1, 0),
(296, 200, '3', '49', 1, 0),
(297, 400, '3', '49', 1, 0),
(298, 100, '3', '48', 1, 0),
(299, 250, '3', '48', 1, 0),
(300, 300, '3', '48', 1, 0),
(302, 400, '3', '48', 1, 0),
(303, 100, '10', '65', 2, 0),
(304, 150, '10', '65', 2, 0),
(305, 200, '10', '65', 2, 0),
(306, 250, '10', '65', 2, 0),
(307, 300, '10', '65', 2, 0),
(308, 350, '10', '65', 2, 0),
(309, 100, '10', '66', 2, 0),
(310, 150, '10', '66', 2, 0),
(311, 200, '10', '66', 2, 0),
(314, 250, '10', '66', 2, 0),
(315, 300, '10', '66', 2, 0),
(317, 150, '10', '55', 2, 0),
(318, 300, '10', '55', 2, 0),
(319, 100, '10', '67', 2, 0),
(320, 200, '10', '67', 2, 0),
(321, 250, '10', '67', 2, 0),
(322, 300, '10', '67', 2, 0),
(323, 350, '10', '67', 2, 0),
(324, 400, '10', '67', 2, 0),
(325, 450, '10', '67', 2, 0),
(326, 500, '10', '67', 2, 0),
(327, 100, '10', '46', 2, 0),
(328, 200, '10', '46', 2, 0),
(329, 250, '10', '46', 2, 0),
(330, 300, '10', '46', 2, 0),
(331, 350, '10', '46', 2, 0),
(332, 400, '10', '46', 2, 0),
(333, 450, '10', '46', 2, 0),
(334, 500, '10', '46', 2, 0),
(335, 100, '10', '45', 2, 0),
(336, 350, '10', '45', 2, 0),
(337, 200, '10', '45', 2, 0),
(338, 400, '10', '45', 2, 0),
(339, 450, '10', '45', 2, 0),
(340, 500, '10', '45', 2, 0),
(341, 100, '10', '47', 2, 0),
(342, 100, '10', '47', 2, 0),
(343, 200, '10', '47', 2, 0),
(344, 250, '10', '47', 2, 0),
(345, 300, '10', '47', 2, 0),
(346, 350, '10', '47', 2, 0),
(347, 400, '10', '47', 2, 0),
(348, 450, '10', '47', 2, 0),
(349, 500, '10', '47', 2, 0),
(351, 150, '10', '52', 2, 0),
(352, 200, '10', '52', 2, 0),
(353, 250, '10', '52', 2, 0),
(354, 350, '10', '52', 2, 0),
(355, 400, '10', '52', 2, 0),
(356, 450, '10', '52', 2, 0),
(357, 500, '10', '52', 2, 0),
(358, 100, '10', '53', 2, 0),
(359, 200, '10', '53', 2, 0),
(360, 300, '10', '53', 2, 0),
(361, 350, '10', '53', 2, 0),
(362, 400, '10', '53', 2, 0),
(363, 450, '10', '53', 2, 0),
(364, 500, '10', '53', 2, 0),
(365, 100, '10', '54', 2, 0),
(366, 150, '10', '54', 2, 0),
(367, 200, '10', '54', 2, 0),
(368, 300, '10', '54', 2, 0),
(369, 350, '10', '54', 2, 0),
(370, 400, '10', '54', 2, 0),
(371, 450, '10', '54', 2, 0),
(372, 500, '10', '54', 2, 0),
(373, 350, '10', '55', 2, 0),
(374, 400, '10', '55', 2, 0),
(375, 450, '10', '55', 2, 0),
(376, 500, '10', '55', 2, 0),
(377, 400, '10', '65', 2, 0),
(378, 450, '10', '65', 2, 0),
(379, 500, '10', '65', 2, 0),
(380, 350, '10', '66', 2, 0),
(381, 400, '10', '66', 2, 0),
(382, 450, '10', '66', 2, 0),
(383, 500, '10', '66', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `register_user`
--

CREATE TABLE `register_user` (
  `id` int(255) NOT NULL,
  `membership_id` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `nominy_name` varchar(255) DEFAULT NULL,
  `nominy_phone` varchar(255) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `register_user`
--

INSERT INTO `register_user` (`id`, `membership_id`, `fname`, `lname`, `email`, `password`, `address`, `phone`, `relation`, `nominy_name`, `nominy_phone`, `created_on`, `status`) VALUES
(1, 'ASC43147', 'shubhanshu', 'soni', 'soni.shubhanshu93@live.com', '123456', NULL, NULL, NULL, NULL, NULL, '2019-01-07 07:03:41', 0),
(2, 'ASC14936', 'rupali', 'tomar', 'rupali@gmail.com', '123456', NULL, NULL, NULL, NULL, NULL, '2019-01-07 07:26:25', 0),
(3, 'ASC49666', 'Jibola ', 'Lawal', 'zeniklawal2000@gmail.com', 'Mayowa@2017', NULL, NULL, NULL, NULL, NULL, '2019-01-08 01:19:23', 0),
(4, 'ASC91037', 'Lofton', 'Tetteh', 'notfol@hotmail.com', '*2185*', '24 Page Avenue', '07533695264', 'Daughter', 'Christie Lerche', '004552302326', '2019-01-08 01:34:21', 0),
(5, 'ASC89169', 'Adam', 'Lofton', 'notfol@outlook.com', '*2185*', NULL, NULL, NULL, NULL, NULL, '2019-01-08 07:40:29', 0),
(6, 'ASC50496', 'Augustine', 'Saliu', 'saliu@sky.com', 'Suwebat16l96j', NULL, NULL, NULL, NULL, NULL, '2019-01-08 10:18:16', 0),
(7, 'ASC51876', 'Ayo', 'Saliu', 'abrahamsaliu@yahoo.com', 'Suwebat16l96j', NULL, NULL, NULL, NULL, NULL, '2019-01-10 01:12:27', 0),
(8, 'ASC56078', 'edison', 'hernandez', 'hheadrian@hotmail.com', 'Cachete18', NULL, NULL, NULL, NULL, NULL, '2019-01-19 09:53:46', 0),
(9, 'ASC55525', 'Ben', 'Tetteh', 'benjtee@aol.com', 'bella', NULL, NULL, NULL, NULL, NULL, '2019-01-23 13:02:51', 0),
(10, 'ASC88052', 'Mahmud', 'Shahajjo', 'mahmud@sahajjo.com', 'r1r2r3r4r5R$', NULL, NULL, NULL, NULL, NULL, '2019-02-06 18:11:43', 0);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `title`, `description`, `name`, `user_id`) VALUES
(7, 'jaman', 'nice rev', 'kaman', 10);

-- --------------------------------------------------------

--
-- Table structure for table `saving_plan`
--

CREATE TABLE `saving_plan` (
  `saving_plan_id` int(255) NOT NULL,
  `saving_plan_name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saving_plan`
--

INSERT INTO `saving_plan` (`saving_plan_id`, `saving_plan_name`, `status`) VALUES
(1, 'individual saving plan', 1),
(2, 'Family & Friends Savings Plan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `settings_type` text,
  `settings_key` text,
  `settings_val` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `settings_type`, `settings_key`, `settings_val`) VALUES
(1, 'stripe', 'test_mode', 'on'),
(2, 'stripe', 'tsk', 'sk_test_iUpR5QqdRZ614gAciTyxsR27'),
(3, 'stripe', 'tpk', 'pk_test_2ulpwzwz8VDV89jAXz7TBJWu 	 	'),
(4, 'stripe', 'lsk', 'sk_live_iUpR5QqdRZ614gAciTyxsR27'),
(5, 'stripe', 'lpk', 'pk_live_2ulpwzwz8VDV89jAXz7TBJWu 	 	'),
(6, 'stripe', 'signing_secret', '');

-- --------------------------------------------------------

--
-- Table structure for table `single_user_selectedplan`
--

CREATE TABLE `single_user_selectedplan` (
  `id` int(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `s_planid` varchar(255) DEFAULT NULL,
  `s_plan_type` varchar(255) DEFAULT NULL,
  `s_plan_period` varchar(255) DEFAULT NULL,
  `s_plan_amount` varchar(255) DEFAULT NULL,
  `group_plan_id` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stripe_customer`
--

CREATE TABLE `stripe_customer` (
  `stripe_customer_serial` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `stripe_customer_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `id_saving_plan` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`, `id_saving_plan`, `status`) VALUES
(2, 'Weekly', 1, 0),
(3, 'Monthly', 1, 0),
(5, 'Fort-night', 2, 0),
(7, 'Yearly', 1, 0),
(8, 'Fort-night', 1, 0),
(9, 'Weekly', 2, 0),
(10, 'Monthly', 2, 0),
(11, 'Yearly', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_join_group`
--

CREATE TABLE `user_join_group` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_join_id` int(11) NOT NULL,
  `stripe_token` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_join_group`
--

INSERT INTO `user_join_group` (`id`, `group_id`, `user_join_id`, `stripe_token`) VALUES
(1, 1, 0, '5c3346f7d55d8');

-- --------------------------------------------------------

--
-- Table structure for table `user_selected_group_plan`
--

CREATE TABLE `user_selected_group_plan` (
  `id` int(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `planid` varchar(255) DEFAULT NULL,
  `plan_type` varchar(255) DEFAULT NULL,
  `plan_period` varchar(255) DEFAULT NULL,
  `plan_amount` varchar(255) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  `groupName` varchar(100) NOT NULL,
  `groupUserName` varchar(255) NOT NULL,
  `stripe_token` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_selected_group_plan`
--

INSERT INTO `user_selected_group_plan` (`id`, `user_id`, `user_name`, `planid`, `plan_type`, `plan_period`, `plan_amount`, `created_on`, `status`, `groupName`, `groupUserName`, `stripe_token`) VALUES
(1, '2', '2', '230', '10', '46', '150', '2019-01-07 07:27:56', 0, 'shrinkcom', '2', '5c3345cc6b0ad'),
(2, '', '', '246', '10', '45', '250', '2019-01-19 01:22:53', 0, '5/250monthly', '3,4,5,6,7', '5c42c23d10aa9');

-- --------------------------------------------------------

--
-- Table structure for table `user_selected_plan`
--

CREATE TABLE `user_selected_plan` (
  `id` int(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `planid` varchar(255) DEFAULT NULL,
  `plan_type` varchar(255) DEFAULT NULL,
  `plan_period` varchar(255) DEFAULT NULL,
  `plan_amount` varchar(255) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stripe_token` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_selected_plan`
--

INSERT INTO `user_selected_plan` (`id`, `user_id`, `user_name`, `planid`, `plan_type`, `plan_period`, `plan_amount`, `created_on`, `stripe_token`, `status`) VALUES
(1, '', '', '234', '3', '7', '100', '2019-01-07 07:21:28', '5c334448b0f09', 0),
(2, '1', '1', '234', '3', '7', '100', '2019-01-07 07:24:05', '5c3344e5b7eb5', 0),
(3, '4', '4', '259', '3', '7', '10', '2019-01-10 05:44:13', '5c3721fdab946', 0),
(4, '8', '8', '259', '3', '7', '10', '2019-01-19 09:59:34', '5c433b5602a1d', 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdraw`
--

CREATE TABLE `withdraw` (
  `id` int(11) NOT NULL,
  `sort_code` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `withdraw`
--

INSERT INTO `withdraw` (`id`, `sort_code`, `name`, `account_number`, `user_id`, `phone`, `country`, `address`, `amount`, `status`) VALUES
(1, NULL, 'rupali', '3454364636', '2', '9407133295', 'India', '7/8, Vijay NAgar Indore', '100', 1),
(2, NULL, 'Shubhanshu Soni', '31683047780', '1', '8989500336', 'India', '28, Vishnupuri Main, Flat No. 202, Anand Vihar Apart. , Bhanwarkua', '100', 1),
(3, NULL, 'Lofton Tetteh', '5460977283133040', '4', '7533695264', 'UK ', '24 Page Avenue HA9 9FP ', '10', 1),
(5, NULL, 'ekta sodawala', '77777777777777777777', '2', '9039241163', 'India', 'test', '20', 1),
(6, '1254', 'ekta sodawala', '77777777777777777777', '2', '9039241163', 'India', 'test', '10', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_user`
--
ALTER TABLE `message_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register_user`
--
ALTER TABLE `register_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saving_plan`
--
ALTER TABLE `saving_plan`
  ADD PRIMARY KEY (`saving_plan_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `single_user_selectedplan`
--
ALTER TABLE `single_user_selectedplan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stripe_customer`
--
ALTER TABLE `stripe_customer`
  ADD PRIMARY KEY (`stripe_customer_serial`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_join_group`
--
ALTER TABLE `user_join_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_selected_group_plan`
--
ALTER TABLE `user_selected_group_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_selected_plan`
--
ALTER TABLE `user_selected_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdraw`
--
ALTER TABLE `withdraw`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(55) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message_user`
--
ALTER TABLE `message_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `period`
--
ALTER TABLE `period`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=384;
--
-- AUTO_INCREMENT for table `register_user`
--
ALTER TABLE `register_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `saving_plan`
--
ALTER TABLE `saving_plan`
  MODIFY `saving_plan_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `single_user_selectedplan`
--
ALTER TABLE `single_user_selectedplan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stripe_customer`
--
ALTER TABLE `stripe_customer`
  MODIFY `stripe_customer_serial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `user_join_group`
--
ALTER TABLE `user_join_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_selected_group_plan`
--
ALTER TABLE `user_selected_group_plan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_selected_plan`
--
ALTER TABLE `user_selected_plan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `withdraw`
--
ALTER TABLE `withdraw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
