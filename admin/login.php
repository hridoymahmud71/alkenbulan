<?php 
  session_start();
  require_once 'php_action/conn.php';

    if(isset($_SESSION['admin_id']))
    {
       // header('location:deshboard.php');   
    }
 $errors = array();
 if($_POST)
 {
        $username = $_POST['username'];
        $password = $_POST['password'];
       // print_r($_POST);die;
        if(empty($username) || empty($password))
        {
            if($username == "")
            {
                $errors[] = "Username is required";
            }
            if($password == "")
            {
                $errors[] = "password is required";
            }
        }
        else
        {

            $sql = mysqli_query($conn,"SELECT * FROM admin WHERE admin_name = '$username' AND password = '".$_POST['password']."'");
           // print_r($sql);die;
            if(mysqli_num_rows($sql) > 0)
            {
                $row = mysqli_fetch_assoc($sql);
                $admin_id =  $row["admin_id"];
                $type =  $row["type"];
                $_SESSION['admin_id'] = $admin_id;
                $_SESSION['type'] = $type;
                //print_r($_SESSION);die;
                header('location:index.php');   
            }
            else
            {
                $errors[] = "Incorrect username/password combination";
            }
        }
 }

?>

<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
	<!-- bootstrap -->
	<link rel="stylesheet" href="assests1/bootstrap/css/bootstrap.min.css">
	<!-- bootstrap theme-->
	<link rel="stylesheet" href="assests1/bootstrap/css/bootstrap-theme.min.css">
	<!-- font awesome -->
	<link rel="stylesheet" href="assests1/font-awesome/css/font-awesome.min.css">
  <!-- custom css -->
  <link rel="stylesheet" href="custom/css/custom.css">	
  <!-- jquery -->
	<script src="assests/jquery/jquery.min.js"></script>
  <!-- jquery ui -->  
  <link rel="stylesheet" href="assests1/jquery-ui/jquery-ui.min.css">
  <script src="assests1/jquery-ui/jquery-ui.min.js"></script>
  <!-- bootstrap js -->
	<script src="assests1/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row vertical">
			<div class="col-md-5 col-md-offset-4">
				<div class="panel panel-primary" style="margin-top: 120px;">
					<div class="panel-heading">
						<h3 class="panel-title">Please Sign in</h3>
					</div>
					
						<div class="messages">
						<?php if($errors) 
							{
								foreach ($errors as $key => $value) 
								{
									echo '<div class="alert alert-warning" role="alert"><i class="glyphicon glyphicon-exclamation-sign"></i>
									'.$value.'</div>';										
								}
							} 
						?>
						</div>

						<form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" id="loginForm">
							<fieldset style="margin-top: 17px;margin-right: 7px;">
							  <div class="form-group">
									<label for="username" class="col-sm-3 control-label">Username</label>
									<div class="col-sm-9">
									  <input type="text" class="form-control" id="username" name="username" placeholder="Username" autocomplete="off" />
									</div>
								</div>
								<div class="form-group">
									<label for="password" class="col-sm-3 control-label">Password</label>
									<div class="col-sm-9">
									  <input type="password" class="form-control" id="password" name="password" placeholder="Password" autocomplete="off" />
									</div>
								</div>								
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
									  <button type="submit" class="btn btn-default"> <i class="glyphicon glyphicon-log-in"></i> Sign in</button>
									</div>
								</div>
							</fieldset>
						</form>
					
					<!-- panel-body -->
				</div>
				<!-- /panel -->
			</div>
			<!-- /col-md-4 -->
		</div>
		<!-- /row -->
	</div>
	<!-- container -->	
</body>
</html>







	