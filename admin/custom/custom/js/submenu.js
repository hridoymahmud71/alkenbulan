var manageSubMenuTable;

$(document).ready(function() {
	// top bar active
	$('#navCategories').addClass('active');
	//alert();
	// manage brand table
	manageSubMenuTable = $("#manageSubMenuTable").DataTable({
		'ajax': 'php_action/fetchsubmenu.php',
		'order': []		
	});

	// submit brand form function
	$("#submitMenuForm").unbind('submit').bind('submit', function() {
		// remove the error text
		$(".text-danger").remove();
		// remove the form error
		$('.form-group').removeClass('has-error').removeClass('has-success');			

		var menuName  = $("#menuName").val();
		var submenuName  = $("#submenuName").val();
		var menuImg = $("#menuImg").val();

if(submenuName  == "") {
			$("#submenuName ").after('<p class="text-danger">Menu is required</p>');
			$('#submenuName ').closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#submenuName ").find('.text-danger').remove();
			// success out for form 
			$("#submenuName ").closest('.form-group').addClass('has-success');	  	
		}
		if(menuName  == "") {
			$("#menuName ").after('<p class="text-danger">Menu is required</p>');
			$('#menuName ').closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#menuName ").find('.text-danger').remove();
			// success out for form 
			$("#menuName ").closest('.form-group').addClass('has-success');	  	
		}

		if(menuImg == "") {
			$("#menuImg").after('<p class="text-danger">Image is required</p>');

			$('#menuImg').closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#menuImg").find('.text-danger').remove();
			// success out for form 
			$("#menuImg").closest('.form-group').addClass('has-success');	  	
		}

		if(menuName && submenuName && menuImg) {
			var form = $(this);
			var formData = new FormData(this);
			// button loading
			$("#createMenuBtn").button('loading');

			$.ajax({
				url : form.attr('action'),
				type: form.attr('method'),
				data: formData,
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				success:function(response) {
					// button loading
					$("#createMenuBtn").button('reset');

					if(response.success == true) {
						// reload the manage member table 
						manageMenuTable.ajax.reload(null, false);						

  	  			// reset the form text
						$("#submitMenuForm")[0].reset();
						// remove the error text
						$(".text-danger").remove();
						// remove the form error
						$('.form-group').removeClass('has-error').removeClass('has-success');
  	  			
  	  			$('#add-menu-messages').html('<div class="alert alert-success">'+
            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
          '</div>');

  	  			$(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					}  // if

				} // /success
			}); // /ajax	
		} // if

		return false;
	}); // /submit brand form function

});

