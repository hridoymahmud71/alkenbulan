<?php

require_once 'php_action/conn.php';

@$eid = $_GET['id'];


?>
<?php

include('header.php');
include_once('../Mahmud_query.php');

$mq = new Mahmud_query();

?>
<!-- /. NAV SIDE  -->
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">
                    Edit Type
                    <small></small>
                </h1>
            </div>
        </div>
        <!-- /. ROW  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit Type
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <?php
                            if (isset($_GET['ys'])) {  // print_r($_GET);die;
                                echo '<div class="alert alert-primary alert-dismissable fade in" role="alert" style="color:Green; font-weight:bold;"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                "Add Trainer successfully!" </div>';

                            }
                            ?>
                            <div class="col-lg-12">
                                <form role="form" action="edittype_action.php" method="POST"
                                      enctype="multipart/form-data">

                                    <input type="hidden" name="editid" value="<?php echo $eid; ?>">
                                    <?php

                                    //$sql = "SELECT * FROM `type` JOIN saving_plan ON type.id_saving_plan =saving_plan.saving_plan_id  WHERE id='$eid'";
                                    $type_sql = "SELECT * FROM `type`   WHERE id='$eid'";
                                    $plans_sql = "SELECT * FROM `saving_plan`";

                                    $type = $mq->row($type_sql);
                                    $plans = $mq->rows($plans_sql);

                                    if (!empty($type)) {
                                        ?>

                                        <!--<pre>
                                            <?php /*print_r($type) */?>
                                            <?php /*print_r($plans) */?>
                                        </pre>-->

                                        <div class="form-group">
                                            <label>Type</label>
                                            <input class="form-control" value="<?php echo $type['type']; ?>"
                                                   type="text" name="type" placeholder="Entertype"
                                                   required="required">
                                            <!--  <p class="help-block">Example block-level help text here.</p> -->
                                        </div>


                                        <div class="form-group">
                                            <label>Select plan Group</label>
                                            <select name="saving_plan_id" class="form-control">

                                                <?php if (!empty($plans)) { ?>
                                                    <?php foreach ($plans as $plan) { ?>

                                                        <option value="<?php echo $plan['saving_plan_id']; ?>" <?php if ($type['id_saving_plan'] == $plan['saving_plan_id']) {
                                                            echo 'selected';
                                                        } ?>><?php echo $plan['saving_plan_name']; ?> </option>

                                                    <?php } ?>
                                                <?php } ?>

                                            </select>
                                        </div>

                                        <!--     <div class="form-group">
                                            <label>Joke Category Image</label>
                                            <input type="file" class="form-control" name="image" placeholder="Enter Event Location">
                                            <img src="vas_api/category_image/<?php echo $row['category_image']; ?> " style="height:100px; weight:100px;">
                                            
                                        </div>   -->
                                        <button type="submit" name="submit" value="submit" class="btn btn-primary">
                                            Submit Button
                                        </button>
                                        <!--  <button type="reset" class="btn btn-default">Reset Button</button> -->


                                        <?php
                                    }

                                    ?>
                                </form>
                            </div>
                            <!-- /.col-lg-6 (nested) -->

                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <footer><p>All right reserved. Template by: <a href="">Shrinkcom software </a></p></footer>
    </div>
    <!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- Bootstrap Js -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Metis Menu Js -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- Custom Js -->
<script src="assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="assets/js/morris/morris.js"></script>


<script src="assets/js/custom-scripts.js"></script>

<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);

</script>
</body>
</html>
