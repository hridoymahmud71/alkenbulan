<?php include('header.php'); ?>

<div class="page-wrapper">
    <?php

    if (isset($_GET['msg1'])) {
        echo '<div class="alert alert-warning">
  <strong>Current Password does not matched!</strong> 
</div>';

    }
    if (isset($_GET['msg2'])) {
        echo '<div class="alert alert-warning">
  <strong> New Password And Confirm Password not matched</strong> 
</div>';

    }
    if (isset($_GET['msg3'])) {
        echo '<div class="alert alert-success">
  <strong>Updated Successfully</strong> 
</div>';

    }

    $query1 = mysqli_query($conn, "SELECT sum(plan_amount) as total FROM `user_selected_plan` WHERE user_id='" . $_SESSION['user_id'] . "'");

    $row1 = mysqli_fetch_assoc($query1);
    if ($row1['total']) {
        $amount1 = $row1['total'];

    } else {
        $amount1 = 0;
    }
    $query2 = mysqli_query($conn, "SELECT sum(plan_amount) as total FROM `user_selected_group_plan` WHERE user_id='" . $_SESSION['user_id'] . "'");

    $row2 = mysqli_fetch_assoc($query2);


    if ($row2['total']) {
        $amount2 = $row2['total'];

    } else {
        $amount2 = 0;
    }

    $query3 = mysqli_query($conn, "SELECT sum(plan_amount) as total FROM `user_selected_group_plan` left join user_join_group on user_join_group.group_id = user_selected_group_plan.id  WHERE user_join_group.user_join_id='" . $_SESSION['user_id'] . "'");

    $row3 = mysqli_fetch_assoc($query3);

    if ($row3['total']) {
        $amount3 = $row3['total'];

    } else {
        $amount3 = 0;
    }

    $total = $amount1 + $amount2 + $amount3;

    echo "[$amount1] [$amount2] [$amount3]";

    ?>
    <div class="container">
        <div class="col-md-12 " style="margin-top:60px; margin-bottom:60px;">
            <form class="form-horizontal" method="POST" action="account_info.php">
                <fieldset>

                    <!-- Form Name -->
                    <legend>withdraw Balance Request</legend>


                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Name">Current Balance</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input name="amount" type="text" placeholder="Phone" class="form-control input-md"
                                       value="  <?php echo $total; ?>" readonly required="required">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-4">

                            <input name="Submit" type="submit" value="withdraw" class="btn btn-rw btn-primary button1">


                        </div>
                    </div>

                </fieldset>
            </form>


        </div>

    </div>

</div>
<!-- End Container fluid  -->
<!-- footer -->
<footer class="footer"> © 2018 All rights reserved. designed by <a href="#">Shrinkcom Softwares</a></footer>
<!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->
<!-- All Jquery -->
<script src="js/lib/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="js/lib/bootstrap/js/popper.min.js"></script>
<script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Menu sidebar -->
<script src="js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->


<!-- Amchart -->
<script src="js/lib/morris-chart/raphael-min.js"></script>
<script src="js/lib/morris-chart/morris.js"></script>
<script src="js/lib/morris-chart/dashboard1-init.js"></script>


<script src="js/lib/calendar-2/moment.latest.min.js"></script>
<!-- scripit init-->
<script src="js/lib/calendar-2/semantic.ui.min.js"></script>
<!-- scripit init-->
<script src="js/lib/calendar-2/prism.min.js"></script>
<!-- scripit init-->
<script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
<!-- scripit init-->
<script src="js/lib/calendar-2/pignose.init.js"></script>

<script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="js/lib/owl-carousel/owl.carousel-init.js"></script>

<!-- scripit init-->

<script src="js/scripts.js"></script>

</body>

</html>