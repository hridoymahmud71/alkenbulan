<?php

  include('header.php');

?>
        
      
   
</div>
<section style="background-image:url(./images/about-banner.jpg);">
	<h1 style="text-align:center; padding: 100px; color:#fff;">About</h1>
</section>	


<section class="section-light" style="padding:50px;">
    <div class="divider-line solid light"></div>
   <div class="container">
      <div class="row sec-padding">
        
       
		<div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 margin-bottom"> <img src="images/piggy.jpg" alt="" class="img-responsive"> </div>
       
        
        <div class="col-md-8 col-sm-6 col-xs-12">
          <div class="text-box white padding-4 margin-bottom">
            <h3 class="uppercase font-weight-b">About Us</h3>
			<p>Alkebulan Savings Club (ASC) was founded by a passionate and an aspiring entrepreneur
who have been consistently seeking alternative ways to improve people&#39;s financial lives. ASC
presents an alternative saving scheme to that provided by the high street banks. Everyone
stands to benefit by joining ASC, however the core group that scheme is beneficial most to
are families with low income earnings, families who are not likely to get loans from the main
stream banks or those who cannot afford the high interest charges.</p>
<p>The Alkebulan Savings Club started in 2005, when the proprietor took the initiative to bring
together five families in need of financial management advice. What started as informal
discussions and encouragement led to running of a savings scheme where these families
could put money away week by week or month by month until they had enough money saved
to buy or spend it on what they had wanted. The scheme which operated within the local
community for many years is now taking on the established banks and providing families
especially low income earners the experience of and manageable savings options together
with first-hand practical financial advice.</p>     
<p>One of our focuses is to advocate equal opportunity. In assisting all our customers regardless of their
race or disability. What we stand for is mutual respect and equal opportunity. You set a target and you
are respectfully welcome to join our club.</p>  
<blockquote>
     <p>Trust is the easiest thing in the world to loose, and the hardest thing in the world to get back.</p>
     <small>by <cite>R. Williams</cite></small>
</blockquote>
        </div>
        
        </div>
	  </div>
         
   
    </div>
  </div>
    </div>
  </div></section>
  <section class="section-side-image section-light clearfix">
      <div class="img-holder col-md-6 col-sm-3 pull-left">
        <div class="background-imgholder" style="background:url(./images/saving2.png);"><img class="nodisplay-image" src="images/saving2.png" alt="" style="width:100%;"> </div>
      </div>
      <div class="container-fluid">
        <div class="row">
       <div class="col-md-6 text-inner-2 pull-right clearfix align-left">
            <div class="text-box white padding-5">
              <div class="ce3-feature-box-7">
                <h2 class="raleway opacity-9">A SUSU HISTORY</h2>
                <div class="clearfix"></div>
				<h5 class=" raleway font-weight-b less-mar-1 uppercase"><mark>BRIEF HISTORY</mark></h2>
<p>A sou-sou (also spelled sou sou, su-su or susu) is an informal rotating savings club, where a group of people get together and contribute an equal amount of money into a fund weekly, bi-weekly or monthly. The total pool, also known as a hand, is then paid to one member of the club on a previously agreed-on schedule. The pool rotates until all members have received their share.
Here is how sou-sous work: The group elects a treasurer who will collect the members’ contributions. She will also create a payout roster, or members can request to receive their hand at any given date during the cycle. Everyone agrees on how much and how often they want to contribute. If ten members are contributing $100 a week, each week a member will receive a $1,000 hand or cash lump sum. The cycle begins again after ten weeks. Any member who can afford it, can also double their contribution and get paid two hands in one cycle. There is no interest to be collected, so you will always get out the exact amount that you put into the pot. Sou-sou, which comes from the Yoruba term “esesu,” originated in West Africa, but is practiced in many African and Caribbean countries. Over the years, sou-sou has evolved, but the basic concept remains the same. Somalis call it “hagbad” or “ayuuto”; in Jamaica, it is known as a “partner”; in Guyana, a “box hand”; Haitians call it a “min”; and if you are Southern African, you may know it as “stokvel.” 
</p><p>The Yoruba esusu was transported over to the New World by African slaves and, while it is little known to African-Americans today, it is still popular among some African, Caribbean, Latino and Asian immigrant communities. Some use it to start businesses, others for big purchases, vacations, down payments on properties and cars and even to send their kids to college. 
As old folks tell it, in the past, housewives who didn’t have an income and those in rural communities who had no access to traditional banks used sou-sous. The women would save a little bit of money from whatever their husbands gave them and put it in a sou-sou to be able to treat themselves when it was their turn to receive a hand. 
This “under the mattress” method of saving may seem archaic for today’s society, but sou-sous can be a useful accountability tool if you do not have the discipline to save on your own. If you need a lump sum but cannot get a credit card or loan from a traditional financial institution due to a bad credit history, a sou-sou may also be your answer. 
Since sou-sous are not regulated by any laws and can, therefore, be risky if someone untrustworthy joins, if you are considering joining one make sure it is with people that you know well and trust. Usually, sou-sou members are from the same family or a close-knit community 
There are no legal paperwork or credit checks involved when starting a sou-sou, all you have to protect you and your money is the familial trust between the members. So pick who you save with wisely..</p>               
				</div>
            </div>
          </div>
        </div>
      </div>
    </section>
			
<?php
  include('footer.php');
?>