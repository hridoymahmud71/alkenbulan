<?php
  include('header.php');
?>



 <!--<a class="btn btn-warning" href="#warning" data-toggle="modal"><h4><i class="glyphicon glyphicon-eye-open"></i> Warning</h4></a>  -->
    <!-- Modal -->
    <div class="modal fade" id="warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-warning">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1><i class="fa fa-lock fa-1x"></i>FORGOT PASSWORD?</h1>
                </div>
                <div class="modal-body">
                    <div class="form-gap"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-4 col-md-offset-1">
					            <div class="panel panel-default">
					              <div class="panel-body">
					                <div class="text-center">
					                  <h3><i class="fa fa-lock fa-4x"></i></h3>
					                  <h2 class="text-center">Forgot Password?</h2>
					                  <p>You can reset your password here.</p>
					                  <div class="panel-body">
					    
					                    <form  action="resetpassword.php" id="register-form" role="form" autocomplete="off" class="form" method="post">
					    
					                      <div class="form-group">
					                        <div class="input-group">
					                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
					                          <input id="email" name="email" placeholder="Email address" class="form-control" required="required"  type="email">
					                        </div>
					                      </div>
					                      <div class="form-group">
					                        <input name="submit" class="btn btn-lg btn-primary btn-block" value="Reset Password" type="submit">
					                      </div>
					                      
					                      <input type="hidden" class="hide" name="token" id="token" value=""> 
					                    </form>
					    
					                  </div>
					                </div>
					              </div>
					            </div>
					          </div>
						</div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal -->		



			<!-- Begin Content -->
			<div class="content-40mg">
				<div class="container">

					<div class="row">
                       <div class="col-sm-3">
							
							   
						</div>
						<!-- Begin Login -->
						<div class="col-sm-6">
							<div class="panel no-margin panel-default">
							    <div class="panel-heading">Login to your account</div>
							    <div class="panel-body">
							        <form role="form" method="POST" action="login_check.php">
							            <div class="form-group">
							                <div class="input-group">
							                    <div class="input-group-addon"><span class="ion-android-mail" style="font-size:9px;"></span></div>
							                    <input class="form-control" type="email" name="email" placeholder="Enter email" required="required">
							                </div>
							            </div>
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="ion-ios7-locked"></span></div>
							                	<input type="password" name="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required="required">
							                </div>
							            </div>
							            <div class="checkbox">
							                <label>
							                  <input type="checkbox" > Remember me
							                <!--  <input type="checkbox" required="required"> Remember me  -->
							                </label>
							            </div>
							            <hr class="mb20 mt15">
							            <button type="submit" name="submit" value="submit" class="btn btn-rw btn-primary">Login</button> &nbsp;&nbsp;&nbsp;<small><a  href="#warning" data-toggle="modal">Forgot your password?</a></small>
							            
							            
					            
							            
							            
							            
							            
							            
							            
							            
							            
							            
							        </form><!-- /form -->
							    </div><!-- /panel body -->
							</div><!-- /panel -->
						</div><!-- /column-->
						<!-- End Login -->
                       <div class="col-sm-3">
							
							   
						</div>
						
					</div><!-- /row -->

				</div><!-- /container -->
			</div><!-- /content -->
			<!-- End Content -->

<?php
  include('footer.php');
?>