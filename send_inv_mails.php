<?php
include_once('Mahmud_email.php');
include_once('Mahmud_utility.php');

function send_inv_mails($user_ids)
{

    $msg = array();
    //sending email to selected members
    foreach ($user_ids as $user_id) {

        $mail_sent = send_email_to_user($user_id);

        $msg[] = $mail_sent['message'];
    }
    return $msg;
}

function send_email_to_user($user_id)
{
    $me = new Mahmud_email();
    $mu = new Mahmud_utility();

    $user = $mu->getUser($user_id);

    /*echo "<pre>";
    print_r($mail_data);
    echo "</pre>";*/
    $email_to = $user['email'];
    //print_r($email_to);


    $mail_data['to'] = $email_to;
    $mail_data['subject'] = "You are invited";
    $mail_data['message'] = "Inviting {$user['fname']} {$user['lname']}";


    return $me->send($mail_data);

}

    ?>